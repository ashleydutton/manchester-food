function navToggle() {
  const navLink = document.querySelector('.site-header__nav-link');
  const navIcon = document.querySelector('.site-header__nav-icon');
  const nav = document.querySelector('.site-header__nav');

  navLink.addEventListener("click", function(e) {
    e.preventDefault();
;
    navIcon.classList.toggle('open');
    nav.classList.toggle('open');
    // body.classList.toggle('open');
  });
}
navToggle();

/*-----------------------------------------*/

window.addEventListener('load', function () {
  Lightense('.lt');
}, false);
